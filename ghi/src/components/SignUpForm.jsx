import { useState } from 'react'
import { Navigate } from 'react-router-dom'

import useAuthService from '../hooks/useAuthService'

export default function SignUpForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const[name, setName] = useState('')
    const[phone_number,setPhoneNumber]=useState('')
    const[is_admin,setIsAdmin]=useState()
    const[active_cart_id,setActiveCart]=useState(null)

    const { signup, user, error } = useAuthService()

    async function handleFormSubmit(e) {
        e.preventDefault()

        setActiveCart(null)

        await signup({username, password,name,phone_number,is_admin,active_cart_id})
    }
    if (user) {
        return <Navigate to="/" />

    }



    return (
        <form onSubmit={handleFormSubmit}>
            {error && <div className="error">{error.message}</div>}
            <input
                type="text"
                name="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                placeholder="Enter Username"
                required
            />
            <input
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Enter Password"
                required
            />
            <input
                type="text"
                name="name"
                value={name}
                onChange={(e)=> setName(e.target.value)}
                placeholder='Enter Name'
                required
            />
              <input
                type="text"
                name="phonenumber"
                value={phone_number}
                onChange={(e)=> setPhoneNumber(e.target.value)}
                placeholder='Enter Phone Number'
                required
            />
              <input
                type="text"
                name="isadmin"
                value={is_admin}
                onChange={(e)=> setIsAdmin(e.target.value)}
                placeholder='Administrator'
                required
            />
            <button type="submit">Sign Up</button>
        </form>
    )
}
