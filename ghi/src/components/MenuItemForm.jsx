import {useState} from 'react'
import { baseUrl} from '../services/authService'
import useAuthService from '../hooks/useAuthService'
import { useNavigate } from 'react-router-dom'



export default function MenuItemForm(){
    const[name,setName]=useState('')
    const[description,setDescription]=useState('')
    const[image,setImage]=useState('')
    const[price,setPrice]=useState(0.0)
    const {user}=useAuthService()
    const navigate= useNavigate()

 async function handleFormSubmit(e){
    e.preventDefault();

    let url =`${baseUrl}/api/menu`
    const response=await fetch (url,{
        method:"POST",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
            name:name,
            description:description,
            image:image,
            price:price,
        }),
    });
     if(!response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

     }
    const new_item = await response.json()
    navigate("/menu")
    return new_item
   }

  return (
    <>
    { user.is_admin ? (
    <form onSubmit={handleFormSubmit}>
        {/* {error && <div className="error">{error.message}</div>} */}

        <input
           type="text"
           name="name"
           value={name}
           onChange={(e)=>setName(e.target.value)}
           placeholder='Enter Item Name'
        />
         <input
           type="text"
           name="description"
           value={description}
           onChange={(e)=>setDescription(e.target.value)}
           placeholder='Description'
        />
        <input
           type="text"
           name="image"
           value={image}
           onChange={(e)=>setImage(e.target.value)}
           placeholder='Image'
        />
         <input
           type="text"
           name="price"
           value={price}
           onChange={(e)=>setPrice(e.target.value)}
           placeholder='Enter Price'
        />

       <button type="Submit"> Submit </button>

    </form>) :(
     <h1> You're not an admin!</h1>
    )}
    </>

  )

}
