import useAuthService from "../hooks/useAuthService"
import { baseUrl } from "../services/authService";


export default function CreateCart(){

const{user}=useAuthService()
 async function handleClick(){

    let url =`${baseUrl}/api/ShoppingCart`
    const response=await fetch (url,{
        method:"POST",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
           users_id:user.id,
        }),
    });
     if(!response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

     }
    const shopping_cart = await response.json()

    return shopping_cart
   }

 return(
        <button onClick={()=> handleClick()}>Create Cart</button>
    )
}
