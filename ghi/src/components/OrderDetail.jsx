import useAuthService from "../hooks/useAuthService"
import { baseUrl } from "../services/authService"
import { useState,useEffect } from "react"
import { useParams } from "react-router-dom"
import { useLocation } from "react-router-dom"

export default function OrderDetail(){
const {user}=useAuthService()
const[orderDetails,SetOrderDetails]= useState([])
const{order_id}= useParams()
const user_id=user.id
const location=useLocation()
const{orderNumber}=location.state

const getOrder=async()=>{
    const url=`${baseUrl}/api/${user_id}/order/${order_id}`
    const response= await fetch(url,
       {
        credentials:'include',

    });
    const data= await response.json();
    if(response.ok){
        SetOrderDetails(data);
    }
 console.log(orderDetails)
}
useEffect(()=>{
    getOrder();
},[])


    return(
      <>

        <h1>Order Detail </h1>

   <ul>



          {orderDetails.map((order,index) => {

            const readableDate = new Date(order.date).toLocaleString("en-US", {
              month: "long",
              day: "2-digit",
              year: "numeric",
              hour: "2-digit",
              minute: "2-digit",
              hour12: true,

            });


            return (
                 <>
         <li key={index}>
        {index===0 && (
         <>
         <li>Order #{orderNumber}</li>
        <p>{readableDate}</p>

        </>
        )}

        <p>
        <li>{order.menu_item_name}</li>

        <li>Amount: {order.total_quantity}</li>

        <li>Price: ${order.item_price}</li>
        </p>
        </li>

        </>


            );
          })}
        </ul>

</>
        )}
















//  Can i set in state  two piece of data( order details + the shoppign cart logic of the yet
// to be written sql snippet returning # iced coffees, total price)

// you know you can write multiple state arrays- so maybe do one to map out order details, or one to map out SC details
// or combine them into one( backend query, or frontend mashup?)
