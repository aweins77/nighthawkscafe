import { baseUrl} from '../services/authService'
import useAuthService from '../hooks/useAuthService'


export default function CreateItem({menu_item_id,buttonLabel="Add to Cart",displayCartItems}){
    const {user}=useAuthService()
    const shopping_cart_id=user.active_cart_id
    const quantity = 1








   async function handleClick(){


     const url =`${baseUrl}/api/ShoppingCart/${shopping_cart_id}/CartItem/`
      const response=await fetch (url,{
        method:"POST",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
            shopping_cart_id:shopping_cart_id,
            menu_item_id:menu_item_id,
            quantity:quantity,

        }),
    });




    if(!response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

     }
    else{

        await displayCartItems();
    }

   }

    return(
        <>

      <button onClick={handleClick}> {buttonLabel}</button>

        </>



    )
}
