import useAuthService from "../hooks/useAuthService"
import { useState,useEffect } from "react"
import { baseUrl } from "../services/authService"
import CreateItem from "./createcartitem"
import { Link } from "react-router-dom"

export default function DisplayCart({showCheckoutButton=true}){
const {user}=useAuthService()
const active_cart_id=user.active_cart_id
const[cartItems,SetCartItems]=useState([])

const displayCartItems=async()=>{
    const url=`${baseUrl}/api/ShoppingCart/${active_cart_id}/listitemdetails`
    const response= await fetch(url, {credentials:'include',
    });

    const data=await response.json();
    if(response.ok){
        SetCartItems(data)

    }


}

useEffect(()=>{
    displayCartItems();
  },[])

const deleteItem=async(shopping_cart_id,menu_item_id)=>{
    const url_delete_item=`${baseUrl}/api/ShoppingCart/${shopping_cart_id}/CartItem/${menu_item_id}`
    const response= await fetch(url_delete_item, {
        method:'DELETE',
        credentials:'include',
    });


     if (response.ok) {
   await  displayCartItems();
  } else {
    console.error("Could not delete items");
  }
  }

  const decrementItem=async(shopping_cart_id,menu_item_id)=>{

    const url_decrement_item=`${baseUrl}/api/ShoppingCart/${shopping_cart_id}/CartItem/${menu_item_id}/decrement`
    const response= await fetch(url_decrement_item,{
      method:'DELETE',
      credentials:'include',
    });

    if(response.ok){
      await displayCartItems();
    }else{
     console.error("Could not delete items");
    }
  }


const calculatesubtotal = () => {
  const subtotal= cartItems.reduce((subtotal, item) => subtotal + parseFloat(item.total_item_price), 0);
  return parseFloat(subtotal.toFixed(2))
};

const calculatetotal=()=>{
    let tax=0.085
    let total= calculatesubtotal()* tax +calculatesubtotal()
    return total.toFixed(2)
   }

    return(

        <>
        <div>
            <h1>Your Cart</h1>
        <ul>
         {cartItems.map((cartitem)=>(
            <li key={cartitem.menu_item_id}>
                <p>{cartitem.menu_item_name}</p>
                <p>Quantity {cartitem.total_quantity}</p>
                <p>Price ${cartitem.total_item_price}</p>
                <button onClick={()=>deleteItem(cartitem.menu_item_id,cartitem.shopping_cart_id)}>Delete Item</button>
                < CreateItem menu_item_id={cartitem.menu_item_id} displayCartItems={displayCartItems} buttonLabel= "+"/>
                <button onClick={()=>decrementItem(cartitem.shopping_cart_id,cartitem.menu_item_id)}> - </button>
        </li>) )}
        </ul>
         <p>Subtotal: ${calculatesubtotal()} </p>
         <p>Total:$ {calculatetotal()}</p>
         {showCheckoutButton && (
          <button> <Link to= '/shoppingcart/:active_cart_id/viewcart/createorder'>Checkout </Link></button>)
         }
        </div>



        </>
    )}





//   const calculatesubtotal=()=>
//    {let subtotal=0;
//     for(let i=0;i<cartItems.length;i++)
//     {
//      subtotal += cartItems[i].total_item_price}

//     return subtotal
//    }

//    array can only be accesed by index
