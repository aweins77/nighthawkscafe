import useAuthService from "../hooks/useAuthService"
import { baseUrl } from "../services/authService"
import { useState,useEffect } from "react"
import {Link} from "react-router-dom"


export default function OrderHistory(){

const[orders,setOrders]= useState([])
const{user}=useAuthService()
const user_id= user.id






const getOrders= async()=>{
const url = `${baseUrl}/api/${user_id}/order`

const response=await fetch(url,

        { method:"GET",
            credentials:"include",});

    const data= await response.json();
    if(response.ok){
        setOrders(data);
    }

}

useEffect(()=>{
    getOrders();
},[] );


return (
    <>
      <div>
        <h1>My Orders</h1>
        <ul>
          {orders.map((order,index) => {

            const readableDate = new Date(order.date).toLocaleString("en-US", {
              month: "long",
              day: "2-digit",
              year: "numeric",
              hour: "2-digit",
              minute: "2-digit",
              hour12: true,
            });

            const order_number=index+1


            return (

              <li key={order.shopping_cart_id}>

               <p> <Link to={`/myorders/${user_id}/order/${order.id}`}state={{orderNumber:order_number}}>
               Order {order_number}</Link> </p>

                <p>{readableDate}</p>

              </li>
            );
          })}
        </ul>


        </div>
       </>
)}
