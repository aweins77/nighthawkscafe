
import { useParams,Link } from 'react-router-dom'
import { baseUrl} from '../services/authService'
import { useEffect, useState } from 'react'
import useAuthService from '../hooks/useAuthService'
import CreateItem from './createcartitem'



export default function MenuItem(){

const{menu_item_id}=useParams()
const[menuItem,setMenuItem]=useState("")
const{user}=useAuthService()


const getItem= async()=>{
    const url=`${baseUrl}/api/menu/${menu_item_id}`
    const response= await fetch(url,
        {credentials:'include',
    });
    const data= await response.json();
    if(response.ok){
        setMenuItem(data);
    }
}

useEffect(()=>{
    getItem();
},[])






return(
    <>
        <div>
        <h2>{menuItem.name}</h2>
        <p>{menuItem.description}</p>
        <img src={menuItem.image} style={{height:100, width:100}}/>
        <p>{menuItem.price}</p>
        </div>
        {user &&
                <CreateItem menu_item_id={menuItem.id} displayCartItems={()=>{}}/>
                }

       {user && user.is_admin &&(
        <button> <Link to={`editmenuitem`}> Edit </Link></button>
    )}


   </>


)}
