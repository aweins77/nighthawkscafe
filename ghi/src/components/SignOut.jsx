import {signout} from '../services/authService'
import { useNavigate } from 'react-router-dom'





 const SignOutButton = ()=> {
    const navigate=useNavigate();


const handleSignout= async()=>{
    try{
        await signout();
        alert("You have been logged out!")
        navigate("/");
    }catch(error){
        console.error("Error is",error);
    }

};


return(

    <button onClick={handleSignout}>
         Signout </button>


);


};
 export default SignOutButton
