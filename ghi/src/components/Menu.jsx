
import { useState ,useEffect} from "react"
import { baseUrl} from '../services/authService'
import { Link } from "react-router-dom"
import CreateItem from "./createcartitem"
import useAuthService from '../hooks/useAuthService'


export default function Menu(){
const[menuItems,setMenuItems]=useState([])
const{user}=useAuthService()



const getMenuItems=async()=>{
    const url= `${baseUrl}/api/menu`
    const response=await fetch(url,
        {credentials:"include",});
    const data= await response.json();
    if(response.ok){
        setMenuItems(data);
    }

}

useEffect(()=>{
    getMenuItems();
},[]);


return(
        <>
        <div>
        <h1>Menu</h1>
        <ul>
            {menuItems.map((item)=>(
            <li key={item.id}>
                <h1><Link to={`/menu/${item.id}`}>{item.name}</Link></h1>
                <p className="bg-red-700">{item.description}</p>
                <img
                src={item.image} style={{height:100, width:100}}
                />
                <p>${item.price}</p>
                {user &&
                <CreateItem menu_item_id={item.id} displayCartItems={()=>{}}/>
                }
            </li>
            )

            )

            }
        </ul>

        </div>
       </>

    )

}
