import { NavLink,Link} from "react-router-dom";
import useAuthService from '../hooks/useAuthService'
import SignOut from "./SignOut";
import { useEffect } from "react";



import '../index.css'
// import { signout } from "services/authService";
export default function Nav(){

  const{user,isLoggedIn}=useAuthService()

// useEffect to rerender Nav?
// useEffect(()=>{
//     Nav();
//   },)



return (

    <nav >
    <ul className="nav" >
    <li >
     <NavLink to="/">
    Home
    </NavLink>
    </li>
    <li >
      <Link to ="menu">
      Menu
      </Link>
    </li>

    {user && isLoggedIn &&(
      <>
      <li>
       <Link to= "shoppingcart/:active_cart_id/viewcart">
        My Shopping Cart
        </Link>
      </li>
      <li >
        <Link to="myorders" >
          My Orders
        </Link>
      </li>
      <li>
        <SignOut/>
      </li>
      </>
    )}

    {user && user.is_admin &&(
      <li >
        <Link to ="createmenuitem">
        Create Menu Item
        </Link>
      </li>
    )

    }


    {!user && !isLoggedIn &&(
      <>
     <li >
      <Link to="signin">
      Sign in
      </Link>

    </li>
    <li >
      <Link to="signup">
      Sign Up
    </Link>
    </li>

    </>)}
     </ul>
     </nav>

);

}
