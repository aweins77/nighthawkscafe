import useAuthService from "../hooks/useAuthService"
import { baseUrl } from "../services/authService"
import DisplayCart from "./DisplayCart"




export default function CreateOrder(){
const{user}=useAuthService()
const shopping_cart_id=user.active_cart_id

async function handleClick(){

    const url=`${baseUrl}/api/${shopping_cart_id}/order`
      const response=await fetch (url,{
        method:"POST",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
            users_id:user.id,
            date:new Date().toISOString(),
            shopping_cart_id:shopping_cart_id,
      }),
    });

    if(!response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

     }

    const order = await response.json()



  const new_cart_url=`${baseUrl}/api/ShoppingCart`
   const cart_response=await fetch (new_cart_url,{
        method:"POST",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
           users_id:user.id,
        }),
    });
     if(!cart_response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);
     }
    const shopping_cart = await response.json()

    return order,shopping_cart

   }







return(
        <>
        <DisplayCart showCheckoutButton={false}/>
        {/* mock payment */}
        <button onClick={handleClick}>Submit Order</button>
        </>
    )
}
