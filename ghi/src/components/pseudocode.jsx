

//I am considering the use of one button (add to cart) that does 3 diffferent things based on  3 conditions;
//  this button would exist on the MenuItemDetail and Menu pages

// All pages can be passed the usercontext

// 1) active cart id=null - creates a shopping cart and creates the first cart item

// 2) there is a shopping cart and that cart_item is in it( meaning a JOIN SQL snippet in queries that associates a shoppingcartid in cart items with activecartid):
// we adjust it's quantity

// 3} there is a shopping cart and that cart_item isn't in it yet (filtered based on cart_item.menu_item_id)


// Shopping carts will become an Order, but there will be no order details- just cart items
// I architected in the migrations file so that each Order must contain a unique Shopping Cart ID so it can't be reused.

// Wondering how to "clear" the shopping cart after user submits the order( an action that creates an order+bulk inserts order details),
// sort of like how a cookies JWT token clears on signing out, without actually deleting the SC instance from the DB.
// Because if it doesn't, I won't be able to create a new order. Shopping Cart 1 must become Shopping Cart X after an Order is created,
// and a Shopping Cart can't be deleted from the DB once its attached to an Order.

// Solution: ActiveCartid, Serialize the ShoppingCart instance which then assigns that id to user's activecartid when on the
// frontend an order is submitted(Order instance created)

// export default function AddToCartButton(){

// const{user}=useAuthService()


    // 1) create a new shopping cart and a cart item

    //  the stringification(.stringify) part of the body seems to be used in conjunction with a useState Hook.
    // can i employ this formula WITHOUT an input field to alter state, as in a JSX form?
//
//    FETCH TO CREATE SHOPPING CART

//   async function CreateShoppingCart;

//     let url =`${baseUrl}/api/ShoppingCart`
//     const response=await fetch (url,{
//         method:"POST",
//         credentials :"include",
//         headers:{
//             "Content-Type":"application/json",
//         },
//         body:JSON.stringify({
//             users_id:users_id
//         }),
//     });
//      if(!response.ok){
//         const errorData = await response.json();
//         throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

//      }
//     const new_cart = await response.json()
//     return new_cart
//    }

//  AND CREATE CART ITEM

    //   async function CreateCartItem;

//     let url =`${baseUrl}/api/CartItem`
//     const response=await fetch (url,{
//         method:"POST",
//         credentials :"include",
//         headers:{
//             "Content-Type":"application/json",
//         },
//         body:JSON.stringify({
        // shopping_cart_id:shopping_cart_id
        // menu_item_id:menu_item_id
        // quantity:quantity
        // tax:tax
// }),
//     });
//      if(!response.ok){
//         const errorData = await response.json();
//         throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

//      }
//     const new_cart_item = await response.json()
//     return new_cart_item
//    }



    // update a cart item



//     return(
//         // !shopping cart?
//         // <button onClick={}>Add to Cart</button>
//         // create cart, populate first shopping cart item

//         // / shoppingcart.cartitem.menu_item_id?{
//         // <button onClick="">Add to Cart</button>
//         // update item quantity} : {

//         // <button onClick= "">Add to Cart</button>
//         // create cart item}


//     )
// }
