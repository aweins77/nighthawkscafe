import {useState,useEffect} from 'react'
import { useParams,useNavigate } from 'react-router-dom'
import { baseUrl} from '../services/authService'
import useAuthService from '../hooks/useAuthService'




export default function EditMenuItemForm(){
const{menu_item_id}=useParams()
const[menuItem,setMenuItem]=useState("")

const getItem= async()=>{
    const url=`${baseUrl}/api/menu/${menu_item_id}`
    const response= await fetch(url,
        {credentials:'include',
    });
    const data= await response.json();
    if(response.ok){
        setMenuItem(data);
    }
}
useEffect(()=>{
    getItem();
},[])


    const[name,setName]=useState(menuItem.name)
    const[description,setDescription]=useState(menuItem.description)
    const[image,setImage]=useState(menuItem.image)
    const[price,setPrice]=useState(menuItem.price)
    const navigate=useNavigate()
    const {user}=useAuthService()



 async function handleFormSubmit(e){
    e.preventDefault();

    let url =`${baseUrl}/api/menu/${menu_item_id}`
    const response=await fetch (url,{
        method:"PATCH",
        credentials :"include",
        headers:{
            "Content-Type":"application/json",
        },
        body:JSON.stringify({
            name:name,
            description:description,
            image:image,
            price:price,
        }),
    });
     if(!response.ok){
        const errorData = await response.json();
        throw new Error(`Server responsed with ${response.status}:${errorData.detail}`);

     }
    const new_item = await response.json()
    navigate("/menu")
    return new_item
   }

  return (
    <>
     <h1> Editing  "{menuItem.name}" </h1>

    <form onSubmit={handleFormSubmit}>
        {/* {error && <div ="error">{error.message}</div>} */}

        <input
           type="text"
           name="name"
           value={name}
           onChange={(e)=>setName(e.target.value)}
           placeholder={menuItem.name}
        />
         <input
           type="text"
           name="description"
           value={description}
           onChange={(e)=>setDescription(e.target.value)}
           placeholder={menuItem.description}
        />
        <input
           type="text"
           name="image"
           value={image}
           onChange={(e)=>setImage(e.target.value)}
           placeholder="Image URL"
        />
        {/* placeholder can't be copy and pasted this negates choosing Patch */}
         <input
           type="text"
           name="price"
           value={price}
           onChange={(e)=>setPrice(e.target.value)}
           placeholder={menuItem.price}
        />

       <button type="Submit"> Submit </button>
{/* is there  a better solution to placeholder having to manually rewrite? Changed to Patch methdd, would like unchanged values to automatically reinsert */}
    </form>
    <div>
        <h2>{menuItem.name}</h2>
        <p>{menuItem.description}</p>
        <img src={menuItem.image}/>
        <p>${menuItem.price}</p>
    </div>
    </>

  )

}
