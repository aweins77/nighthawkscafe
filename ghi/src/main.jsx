import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import Home from './components/Home'
import SignInForm from './components/SignInForm'
import SignUpForm from './components/SignUpForm'
import MenuItemForm from './components/MenuItemForm'
import App from './App'
import AuthProvider from './components/AuthProvider'
import Menu from './components/Menu'
import MenuItem from './components/MenuItemDetail'
import EditMenuItemForm from './components/EditMenuItemForm'
import Nav from './components/Nav'
import OrderHistory from './components/OrderHistory'
import CreateItem from './components/createcartitem'
import DisplayCart from './components/DisplayCart'
import CreateCart from './components/Createshoppingcart'
import CreateOrder from './components/Checkout'
import OrderDetail from './components/OrderDetail'
import './index.css'

const BASE_URL = import.meta.env.BASE_URL
if (!BASE_URL) {
    throw new Error('BASE_URL is not defined')
}

const router = createBrowserRouter(
    [
        {

            element: <App />,
            children: [

                {
                    path:'/',
                    element: <Home />,
                },
                {
                    path: 'signup',
                    element: <SignUpForm />,
                },

                {
                    path: 'signin',
                    element: <SignInForm />,
                },
                {
                    path:'createmenuitem',
                    element:<MenuItemForm />,
                },

                {
                    path:'menu',
                    element:<Menu/>,


                },
                {
                    path:'menu/:menu_item_id',
                    element:<MenuItem/>,
                },

                {   path: 'menu/:menu_item_id/editmenuitem',
                    element:<EditMenuItemForm />,

                },

                {
                    path:'createitem',
                    element:<CreateItem/>,
                },
                {
                    path:'shoppingcart',
                    element:<CreateCart/>,

                },
                {
                    path:'shoppingcart/:active_cart_id/viewcart',
                    element:<DisplayCart/>,
                },
                {
                    path:'shoppingcart/:active_cart_id/viewcart/createorder',
                    element:<CreateOrder/>,
                },
                {
                    path: 'myorders',
                    element:<OrderHistory/>,
                },
                {
                    path:'myorders/:user_id/order/:order_id',
                    element:<OrderDetail/>,
                }




            ],
        },
    ],
    {
        basename: BASE_URL,
    }
)

const rootElement = document.getElementById('root')
if (!rootElement) {
    throw new Error('root element was not found!')
}

// Log out the environment variables while you are developing and deploying
// This will help debug things
console.table(import.meta.env)

const root = ReactDOM.createRoot(rootElement)
root.render(
    <React.StrictMode>
        <AuthProvider>
            <RouterProvider router={router} />
        </AuthProvider>
    </React.StrictMode>
)
