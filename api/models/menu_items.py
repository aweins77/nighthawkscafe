from pydantic import BaseModel


class MenuItemIn(BaseModel):
    name:str
    description: str
    image: str
    price:float

class MenuItemOut(BaseModel):
    id:int
    name:str
    description:str
    image:str
    price:float
