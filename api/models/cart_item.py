from pydantic import BaseModel
from decimal import Decimal



class CartItemIn(BaseModel):
    shopping_cart_id:int
    menu_item_id:int
    quantity:int






class CartItemOut(BaseModel):
    id:int
    shopping_cart_id:int
    menu_item_id:int
    quantity:int


class CartItemDetails(BaseModel):
    menu_item_id:int
    menu_item_name:str
    total_quantity:int
    total_item_price:Decimal
    shopping_cart_id:int
