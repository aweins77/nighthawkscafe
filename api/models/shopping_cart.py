from pydantic import BaseModel
from typing import List


class ShoppingCartIn(BaseModel):
    users_id:int


class ShoppingCartOut(BaseModel):
    id:int
    users_id: int
