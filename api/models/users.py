"""
Pydantic Models for Users.
"""
from pydantic import BaseModel,Field
from typing import Optional


class UserRequest(BaseModel):
    """
    Represents a the parameters needed to create a new user
    """

    username: str
    password: str
    name: str
    phone_number: str
    is_admin: bool = Field(False)
    active_cart_id: Optional[int]=None


class UserResponse(BaseModel):
    """
    Represents a user, with the password not included
    """

    id: int
    username: str
    name: str
    phone_number: str
    is_admin: bool
    active_cart_id:Optional[int]=None

class OldUserRequest(BaseModel):

    username:str
    password:str

class OldUserResponse(BaseModel):

    id: int
    username:str

class UserWithPw(BaseModel):
    """
    Represents a user with password included
    """

    id: int
    username: str
    password: str
    name: str
    phone_number: str
    is_admin: bool
    active_cart_id: Optional[int]=None
