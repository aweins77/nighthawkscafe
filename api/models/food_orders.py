from pydantic import BaseModel
from datetime import datetime
from typing import List
from decimal import Decimal

class FoodOrderIn(BaseModel):
    users_id:int
    date:datetime
    shopping_cart_id:int



class FoodOrderOut(BaseModel):
    id:int
    users_id:int
    date: datetime
    shopping_cart_id:int


class FoodOrderDetails(BaseModel):
   food_order_id:int
   user_id:int
   date:datetime
   shopping_cart_id:int
   menu_item_name:str
   total_quantity: int
   item_price:Decimal
