from fastapi import APIRouter,Depends,HTTPException,status
from queries.menu_items import MenuQueries,MenuItemIn,MenuItemOut
from utils.exceptions import MenuItemDatabaseException
from utils.authentication import try_get_jwt_user_data, JWTUserData
from typing import Optional


router=APIRouter(tags=["Menu Items"], prefix="/api/menu")


@router.post("",response_model=MenuItemOut)
def create_menu_item(
    menu_item:MenuItemIn,
    menu_item_queries:MenuQueries=Depends(),
    user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
) ->  MenuItemOut:

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to create menu item!"
        )
    try:
        new_menu_item=menu_item_queries.create_menu_item(menu_item)
    except MenuItemDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,detail=str(e)

        )
    return new_menu_item

@router.patch("/{id}",response_model=MenuItemOut)
def update_item(
    id:int,
    menu_item:MenuItemIn,
    queries:MenuQueries=Depends(),
    user:Optional[JWTUserData]=Depends(try_get_jwt_user_data,)
)->MenuItemOut:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to create menu item!"
        )
    try:
        updated_item = queries.update(id,menu_item)
    except MenuItemDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,detail=str(e)

        )
    return updated_item

@router.get("",response_model=list[MenuItemOut])
def get_all_items(
    queries:MenuQueries=Depends(),
) -> list[MenuItemOut]:
    try:
        all_menu_items=queries.get_all_menu_items()
    except MenuItemDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return all_menu_items

@router.get("/{id}", response_model=MenuItemOut)
def get_one_item(
    id:int,
    queries:MenuQueries=Depends()
    )->MenuItemOut:
    menu_item=queries.get_menu_item_detail(id)
    if not menu_item:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Menu Item {id} could not be found",
        )
    return menu_item

@router.delete("/{id}",response_model=bool)
def delete_item(
    id:int,
    queries:MenuQueries=Depends(),
    user:JWTUserData= Depends(try_get_jwt_user_data),
)-> bool:
     if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to delete item!")
     return queries.delete_menu_item(id)
