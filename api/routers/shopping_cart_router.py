from fastapi import APIRouter,Depends,HTTPException,status
from queries.shopping_cart import ShoppingCartQueries,ShoppingCartIn,ShoppingCartOut
from utils.exceptions import ShoppingCartDetailException
from utils.authentication import try_get_jwt_user_data, JWTUserData
from typing import Optional



router=APIRouter(tags=["Shopping Cart"], prefix="/api/ShoppingCart")

@router.post("",response_model=ShoppingCartOut)
def create_shopping_cart(
    shopping_cart:ShoppingCartIn,
    shopping_cart_queries:ShoppingCartQueries=Depends(),
    user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
)->ShoppingCartOut:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to create a Cart"
        )
    try:
      new_cart=shopping_cart_queries.create_shopping_cart(shopping_cart)

    except ShoppingCartDetailException as e:
      raise HTTPException(
          status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
      )
    return new_cart
