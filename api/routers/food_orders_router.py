from fastapi import APIRouter,Depends,HTTPException,status
from queries.food_orders import FoodOrderQueries,FoodOrderIn,FoodOrderOut,FoodOrderDetails
from utils.exceptions import FoodOrderDatabaseException
from utils.authentication import try_get_jwt_user_data, JWTUserData
from typing import Optional, List



router=APIRouter(tags=["Orders"])

@router.post("/api/{shopping_cart_id}/order", response_model=FoodOrderOut)
def create_order(
  order:FoodOrderIn,
  order_queries:FoodOrderQueries=Depends(),
  user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
 )-> FoodOrderOut:
  if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to create an order"
        )
  try:
    new_order=order_queries.create_food_order(order)
  except FoodOrderDatabaseException as e:
      raise HTTPException(
          status_code=status.HTTP_400_BAD_REQUEST, detail =str(e)
      )
  return new_order



# @router.get("/api/{user_id}/order/{id}",response_model=FoodOrderOut)
# def get_order(
#     user_id:int,
#     id:int,
#     queries:FoodOrderQueries=Depends(),
#     user:JWTUserData=Depends(try_get_jwt_user_data),
# )-> FoodOrderOut:
#     if not user:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail= "Not allowed to view order"
#         )
#     try:
#         food_order=queries.get_food_order(id,user_id)
#     except FoodOrderDatabaseException as e:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND
#         )
#     return food_order


@router.get("/api/{user_id}/order/{order_id}",response_model=List[FoodOrderDetails])
def get_order_details(
    user_id:int,
    order_id:int,
    queries:FoodOrderQueries=Depends(),
    user:JWTUserData=Depends(try_get_jwt_user_data),
)-> FoodOrderDetails:
      if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail= "Not allowed to view orders"

        )
      try:
        detail_orders=queries.get_food_order_detail(user_id,order_id)
      except FoodOrderDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
      return detail_orders


@router.get("/api/{user_id}/order",response_model=List[FoodOrderOut])
def get_all_orders(
    user_id:int,
    queries:FoodOrderQueries=Depends(),
    user:JWTUserData=Depends(try_get_jwt_user_data),
)->list[FoodOrderOut]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail= "Not allowed to view orders"

        )
    try:
        all_orders=queries.get_all_orders(user_id)
    except FoodOrderDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )
    return all_orders


@router.delete("/api/{user_id}/order/{id}", response_model=bool)
def delete_order(
    id:int,
    queries:FoodOrderQueries=Depends(),
    user:JWTUserData=Depends(try_get_jwt_user_data),

)-> bool:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to delete order!")
    return queries.delete(id)
