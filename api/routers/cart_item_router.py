from fastapi import APIRouter,Depends,HTTPException,status
from queries.cart_item import CartItemIn,CartItemOut,CartItemQueries,CartItemDetails
from utils.exceptions import CartItemDetailException
from utils.authentication import try_get_jwt_user_data, JWTUserData
from typing import Optional,List



router=APIRouter(tags=["Cart Item"], prefix="/api/ShoppingCart")

@router.post("/{shopping_cart_id}/CartItem/",response_model=CartItemOut)
def create_cart_item(
    shopping_cart_id:int,
    cart_item:CartItemIn,
    cart_item_queries:CartItemQueries=Depends(),
    user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
)->CartItemOut:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to create a Cart Item"
        )
    try:

      new_cart_item=cart_item_queries.create_cart_item(shopping_cart_id,cart_item)

    except CartItemDetailException as e:
      raise HTTPException(
          status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
      )
    return new_cart_item


@router.patch('/{id}',response_model=CartItemOut)
def update_cart_item(
   id:int,
   cart_item:CartItemIn,
   queries:CartItemQueries=Depends(),
   user:Optional[JWTUserData]=Depends(try_get_jwt_user_data,)
   )->CartItemOut:
      if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to update shopping cart item!"
        )
      try:
        updated_item = queries.update_quantity(id,cart_item)
      except CartItemDetailException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,detail=str(e)

        )
      return updated_item

# @router.get('/{shopping_cart_id}/listitems',response_model=List[CartItemOut])
# def get_all_cart_items(
#    shopping_cart_id:int,
#    queries:CartItemQueries=Depends(),
#    user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
# )->List[CartItemOut]:
#     if not user:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Not allowed to access details"
#         )
#     try:
#         shopping_list=queries.get_all_cart_items(shopping_cart_id)
#     except CartItemDetailException as e:
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
#         )
#     print(shopping_cart_id)
#     return shopping_list

@router.get('/{active_cart_id}/listitemdetails',response_model=List[CartItemDetails])
def get_item_details(
   active_cart_id:int,
   queries:CartItemQueries=Depends(),
   user:Optional[JWTUserData]=Depends(try_get_jwt_user_data),
)->List[CartItemDetails]:
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to access details"
        )
    try:
        detail_list=queries.get_items_in_cart(active_cart_id)
    except CartItemDetailException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        )
    return detail_list

@router.delete("/{shopping_cart_id}/CartItem/{menu_item_id}/decrement",response_model=bool)
def decrement_cart_item(
   shopping_cart_id:int,
   menu_item_id:int,
   queries:CartItemQueries=Depends(),
   user:JWTUserData= Depends(try_get_jwt_user_data),
)-> bool:
   if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to delete item!")
   return queries.decrement_cart_item(menu_item_id, shopping_cart_id,)


@router.delete("/{shopping_cart_id}/CartItem/{menu_item_id}",response_model=bool)
def delete_cart_items(
   shopping_cart_id:int,
   menu_item_id:int,
   queries:CartItemQueries=Depends(),
   user:JWTUserData= Depends(try_get_jwt_user_data),
)-> bool:
   if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not allowed to delete item!")
   return queries.delete_cart_items(menu_item_id,shopping_cart_id,)
