steps=[

    [
      """
      ALTER TABLE cart_item ALTER COLUMN quantity
      SET DEFAULT 1
      """,

      """
     ALTER TABLE cart_item ALTER COLUMN quantity
     DROP DEFAULT
      """,

    ],
]
