steps=[

    [
      """
      ALTER TABLE users ALTER COLUMN active_cart_id
      SET DEFAULT NULL
      """,

      """
     ALTER TABLE users ALTER COLUMN active_cart_id
     DROP DEFAULT
      """,

    ],
]
