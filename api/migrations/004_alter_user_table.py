steps = [
    [
        """
        ALTER TABLE users
        ADD name VARCHAR (30) NOT NULL,
        ADD phone_number TEXT NOT NULL,
        ADD is_admin BOOL NOT NULL;


        """,
        """
        DROP COLUMN name,
        DROP COLUMN phone_number,
        DROP COLUMN is_admin;
        """


    ],

]
