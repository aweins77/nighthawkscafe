steps=[
    [
     """
     CREATE TABLE shopping_cart(
     id SERIAL PRIMARY KEY NOT NULL,
     users_id INT,
     FOREIGN KEY(users_id) REFERENCES users(id)
     );

     """,
     """
     DROP TABLE shopping_cart;
     """,

    ],
    [
     """
     CREATE TABLE cart_item(
     id SERIAL PRIMARY KEY NOT NULL,
     shopping_cart_id INT,
     menu_item_id INT,
     quantity INT NOT NULL,
     tax DEC(4,3) DEFAULT 0.085,
     FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart(id),
     FOREIGN KEY(menu_item_id) REFERENCES menu_item(id)

     )
     """,
     """
     DROP TABLE cart_item;
     """,

    ],
    [
     """
     ALTER TABLE food_order
     ADD shopping_cart_id INT UNIQUE,
     ADD CONSTRAINT fk_shopping_cart
     FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart(id);

     """,
     """
     DROP COLUMN shopping_cart_id;

     """,

    ],
]
