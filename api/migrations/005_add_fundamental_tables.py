steps = [
   [
    """
    CREATE TABLE menu_item (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    image TEXT NOT NULL,
    price DECIMAL (4,2) NOT NULL

    );
    """,

    """
    DROP TABLE menu_item;
    """

   ],
   [
    """
    CREATE TABLE food_order (
    id SERIAL PRIMARY KEY NOT NULL,
    users_id INT,
    FOREIGN KEY (users_id) REFERENCES users(id)


    );
    """,
    """
    DROP TABLE food_order;

    """,
   ],
   [
    """
    CREATE TABLE order_detail (
    id SERIAL PRIMARY KEY NOT NULL,
    food_order_id INT,
    menu_item_id INT,
    quantity INT NOT NULL,
    tax DEC (4,3) DEFAULT 0.085,
    FOREIGN KEY (food_order_id)REFERENCES food_order(id),
    FOREIGN KEY (menu_item_id)REFERENCES menu_item(id)

    );

    """,
    """
    DROP TABLE order_detail;

    """,


   ]


]
