steps=[
    [
    """
    ALTER TABLE food_order
    ADD date TIMESTAMP NULL DEFAULT(NOW() AT TIME ZONE 'GMT-4');

    """,

    """
    DROP COLUMN date;

    """,



    ],
]
