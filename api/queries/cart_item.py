import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.cart_item import CartItemIn,CartItemOut,CartItemDetails
from utils.exceptions import UserDatabaseException,CartItemDetailException
from typing import List


DATABASE_URL=os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool=ConnectionPool(DATABASE_URL)

class CartItemQueries:
    def create_cart_item(self,shopping_cart_id:int,CartItem:CartItemIn,)->CartItemOut:
        try:
         with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(CartItemOut))as cur:
                    cur.execute(
                        """
                        INSERT INTO cart_item (
                          shopping_cart_id,
                          menu_item_id,
                          quantity


                        ) VALUES (
                            %s,
                            %s,
                            %s
                        )
                        RETURNING *;
                        """,
                        [
                            shopping_cart_id,
                            CartItem.menu_item_id,
                            CartItem.quantity,

                        ],
                    )

                    cart_item = cur.fetchone()
                    if not cart_item:
                        raise UserDatabaseException(
                          "Could not create cart item"
                        )
        except psycopg.Error:
            raise CartItemDetailException(
                "Could not create Cart item"
            )

        return cart_item
    def update_quantity(self,cart_item_id:int, CartItem:CartItemIn)->CartItemOut:
        try:
         with pool.connection() as conn:
            with conn.cursor(row_factory=class_row(CartItemOut)) as cur:
                cur.execute(
                        """
                        UPDATE cart_item
                        SET quantity=%s
                        WHERE id=%s
                        RETURNING *
                        """,
                        [
                            CartItem.quantity,
                            cart_item_id,
                        ],
                    )
                cart_item = cur.fetchone()
        except psycopg.Error:
         raise CartItemDetailException(
                "Could not update cart item"
            )
        return cart_item



    def get_items_in_cart(self,active_cart_id:int)->List[CartItemDetails]:
       try:
         with pool.connection() as conn:
            with conn.cursor(row_factory=class_row(CartItemDetails)) as cur:
                cur.execute(
                        """
                        SELECT shopping_cart.id AS shopping_cart_id,
                        menu_item.id AS menu_item_id,
                        menu_item.name as menu_item_name,
                        users.active_cart_id as active_cart_id,
                        Sum(cart_item.quantity) as total_quantity,
                        Sum(cart_item.quantity * menu_item.price) as total_item_price
                        from cart_item
                        join shopping_cart on cart_item.shopping_cart_id=shopping_cart.id
                        join menu_item on cart_item.menu_item_id=menu_item.id
                        join users on shopping_cart.id=users.active_cart_id
                        where users.active_cart_id = %s
                        group by shopping_cart.id, menu_item.id, menu_item.name,users.active_cart_id
                        """,
                        [active_cart_id],
                    )

                items = cur.fetchall()

       except psycopg.Error as e:
        print(f"Database error: {e}")
        raise CartItemDetailException(
                "Could not get all items in cart"
            )


       return items


    def delete_cart_items(self,shopping_cart_id:int,menu_item_id:int,)->bool:
       try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(CartItemOut)) as cur:
                cur.execute(
                   """
                   DELETE FROM cart_item
                   WHERE menu_item_id = %s
                   AND shopping_cart_id= %s
                   """,
                   [menu_item_id,shopping_cart_id],
                )
                return True
       except Exception as e:
          print(f"Error deleting cart item: {e}")
          return False




    def decrement_cart_item(self, shopping_cart_id:int, menu_item_id:int,)->bool:
       try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(CartItemOut)) as cur:
                cur.execute(
                   """
                   WITH result AS(
                   SELECT id
                   FROM cart_item
                   WHERE shopping_cart_id = %s
                   AND menu_item_id= %s
                   LIMIT 1
                   )
                   Delete from cart_item
                   Where id IN (select id from result);
                 """,
                   [menu_item_id,shopping_cart_id],
                )
                return True
       except Exception as e:
          print(f"Error deleting cart item: {e}")
          return False



 # def get_all_cart_items(self,shopping_cart_id:int)->List[CartItemOut]:
    #  try:
    #      with pool.connection() as conn:
    #         with conn.cursor(row_factory=class_row(CartItemOut)) as cur:
    #             cur.execute(
    #                     """
    #                     SELECT *
    #                     FROM cart_item
    #                     WHERE shopping_cart_id = %s
    #                     """,
    #                     [shopping_cart_id],
    #                 )
    #             cart_items = cur.fetchall()
    #  except psycopg.Error:
    #      raise CartItemDetailException(
    #             "Could not get all items in cart"
    #         )
    #  return cart_items
