import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.shopping_cart import ShoppingCartIn,ShoppingCartOut
from utils.exceptions import UserDatabaseException, ShoppingCartDetailException


DATABASE_URL=os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool=ConnectionPool(DATABASE_URL)

class ShoppingCartQueries:
    def create_shopping_cart(self,ShoppingCart:ShoppingCartIn)->ShoppingCartOut:
        try:
         with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(ShoppingCartOut) )as cur:
                    cur.execute(
                        """
                        INSERT INTO shopping_cart (
                          users_id
                        ) VALUES (
                            %s
                        )
                        RETURNING *;
                        """,
                        [
                            ShoppingCart.users_id
                        ],
                    )
                    shopping_cart = cur.fetchone()
                    if not shopping_cart:
                        raise UserDatabaseException(
                        "Could not create shopping cart"
                        )
                    cur.execute(
                        """
                        UPDATE users
                        SET active_cart_id = %s
                        WHERE id = %s;
                        """,
                        [shopping_cart.id, ShoppingCart.users_id],
                    )
        except psycopg.Error:
            raise ShoppingCartDetailException(
                "Could not create shopping cart"
            )

        return shopping_cart
