import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.menu_items import MenuItemIn, MenuItemOut
from utils.exceptions import UserDatabaseException, MenuItemDatabaseException


DATABASE_URL=os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool=ConnectionPool(DATABASE_URL)


class MenuQueries:

  def create_menu_item(self,MenuItem:MenuItemIn)->MenuItemOut:
     try:
         with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(MenuItemOut)) as cur:
                    cur.execute(
                        """
                        INSERT INTO menu_item (
                           name,
                           description,
                           image,
                           price
                        ) VALUES (
                            %s, %s,%s,%s
                        )
                        RETURNING *;
                        """,
                        [
                            MenuItem.name,
                            MenuItem.description,
                            MenuItem.image,
                            MenuItem.price,
                        ],
                    )
                    menu_item = cur.fetchone()
                    if not menu_item:
                        raise UserDatabaseException(
                            f"Could not create menu item {MenuItem.name}"
                        )
     except psycopg.Error:
            raise MenuItemDatabaseException(
                f"Could not create menu item{MenuItem.name}"
            )
    #  explore the errors, and the if not/ except blocks
     return menu_item


  def get_all_menu_items(self) -> list[MenuItemOut]:
      try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(MenuItemOut)) as cur:
                cur.execute(
                   """
                   SELECT
                     *
                   FROM menu_item

                   """
                )
                menu_items=cur.fetchall()
      except psycopg.Error as e:
          raise MenuItemDatabaseException("Error getting items")
      return menu_items


  def get_menu_item_detail(self,id:int) -> MenuItemOut:
      try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(MenuItemOut)) as cur:
                cur.execute(
                   """
                   SELECT
                     id,
                     name,
                     description,
                     image,
                     price
                   FROM menu_item
                   WHERE id = %s
                   """,
                   [id],
                )
                menu_item=cur.fetchone()
      except psycopg.Error:
          raise MenuItemDatabaseException("Error getting item")
      return menu_item

  def update(self,menu_id:int,MenuItem:MenuItemIn)->MenuItemOut:
      try:
         with pool.connection() as conn:
            with conn.cursor(row_factory=class_row(MenuItemOut)) as cur:
                cur.execute(
                        """
                        UPDATE menu_item
                        SET name=%s,
                        description =%s,
                        image=%s,
                        price=%s
                        WHERE id=%s
                        RETURNING *
                        """,
                        [
                            MenuItem.name,
                            MenuItem.description,
                            MenuItem.image,
                            MenuItem.price,
                            menu_id,
                        ],
                    )
                menu_item = cur.fetchone()
      except psycopg.Error:
        raise MenuItemDatabaseException(
                f"Could not create menu item {MenuItem.name}"
            )
      return menu_item

  def delete_menu_item(self,id:int)->bool:
      try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(MenuItemOut)) as cur:
                cur.execute(
                   """
                   DELETE FROM menu_item
                   WHERE id = %s
                   """,
                   [id]
                )
                return True
      except Exception as e:
          return False
