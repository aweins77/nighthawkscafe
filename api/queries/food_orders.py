import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from models.food_orders import FoodOrderIn,FoodOrderOut, FoodOrderDetails
from utils.exceptions import UserDatabaseException,FoodOrderDatabaseException
from typing import List


DATABASE_URL=os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")

pool=ConnectionPool(DATABASE_URL)

class FoodOrderQueries:
    def create_food_order(self,Order:FoodOrderIn)-> FoodOrderOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(FoodOrderOut)) as cur:
                    cur.execute(
                        """
                        INSERT INTO food_order (
                           users_id,
                           date,
                           shopping_cart_id
                        ) VALUES (
                            %s,%s,%s
                        )
                        RETURNING *;
                        """,
                        [
                            Order.users_id,
                            Order.date,
                            Order.shopping_cart_id,

                        ],
                        )
                    order =cur.fetchone()
        except psycopg.Error:
            raise FoodOrderDatabaseException(
                "Could not create order"
            )
        return order



    def get_food_order_detail(self,user_id:int,order_id:int)->List[FoodOrderDetails]:
        try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(FoodOrderDetails)) as cur:
                cur.execute(
                   """
                   WITH cart_totals AS (
                        SELECT
                            shopping_cart.id AS shopping_cart_id,
                            menu_item.id AS menu_item_id,
                            menu_item.name AS menu_item_name,
                            SUM(cart_item.quantity) AS total_quantity,
                            menu_item.price AS price
                        FROM
                            cart_item
                        JOIN
                            shopping_cart ON cart_item.shopping_cart_id = shopping_cart.id

                        JOIN
                            menu_item ON cart_item.menu_item_id = menu_item.id
                        GROUP BY
                            shopping_cart.id, menu_item.id, menu_item.name, menu_item.price
                    )

                    SELECT
                        cart_totals.shopping_cart_id,
                        cart_totals.menu_item_name,
                        cart_totals.total_quantity,
                        cart_totals.price AS item_price,
                        users.id AS user_id,
                        food_order.id AS food_order_id,
                        food_order.date AS date
                    FROM
                        cart_totals
                    JOIN
                        shopping_cart ON cart_totals.shopping_cart_id = shopping_cart.id
                    JOIN
                        users ON shopping_cart.users_id = users.id
                    JOIN
                        food_order ON food_order.shopping_cart_id = shopping_cart.id
                    Where users.id= %s
                    and food_order.id=%s
                    ORDER BY
                        cart_totals.shopping_cart_id, cart_totals.menu_item_name;


                   """,
                [user_id,order_id],


                )
                food_order_detail=cur.fetchall()
                print(f"Params: {user_id,order_id}")


        except psycopg.Error:
          raise FoodOrderDatabaseException("Error getting item")
        return food_order_detail


    def get_food_order(self,id:int,user_id:int)->FoodOrderOut:
        try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(FoodOrderOut)) as cur:
                cur.execute(
                   """
                   SELECT
                     id,
                     users_id,
                     date,
                     shopping_cart_id
                   FROM food_order
                   WHERE id = %s
                   AND users_id= %s
                   """,
                   [id,
                    user_id,
                    ],
                )
                food_order=cur.fetchone()
        except psycopg.Error:
          raise FoodOrderDatabaseException("Error getting item")
        return food_order




    def get_all_orders(self,user_id:int)-> list[FoodOrderOut]:
        try:
          with pool.connection() as conn:
              with conn.cursor(row_factory=class_row(FoodOrderOut)) as cur:
                cur.execute(
                   """
                   SELECT * from food_order
                   WHERE users_id= %s
                   ORDER BY date ASC
                   """,
                   [user_id],
                )
                all_orders=cur.fetchall()
        except psycopg.Error as e:
           raise FoodOrderDatabaseException("Error getting items")
        return all_orders


    def delete(self,id:int)-> bool:
       try:
          with pool.connection() as conn:
             with conn.cursor(row_factory=class_row(FoodOrderOut)) as cur:
                cur.execute(
                   """
                   DELETE from food_order
                   WHERE id= %s
                   """,
                   [id]
                )
                return True
       except Exception as e:
          return False
