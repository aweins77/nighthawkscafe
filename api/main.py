"""
Entry point for the FastAPI Application
"""
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import auth_router,menu_items_router,food_orders_router,shopping_cart_router,cart_item_router
import os

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:5173",)],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth_router.router)
app.include_router(menu_items_router.router)
app.include_router(food_orders_router.router)
app.include_router(shopping_cart_router.router)
app.include_router(cart_item_router.router)

@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 25,
            "hour": 19,
            "min": "00"
        }
    }
