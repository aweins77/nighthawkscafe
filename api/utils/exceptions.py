"""
Custom Exceptions for the App
"""


class UserDatabaseException(Exception):
    pass

class MenuItemDatabaseException(Exception):
    pass

class FoodOrderDatabaseException(Exception):
    pass

class OrderDetailException(Exception):
    pass

class ShoppingCartDetailException(Exception):
    pass


class CartItemDetailException(Exception):
    pass
